Rails.application.routes.draw do
 
  root 'static_pages#home'
  get '/about',           to: 'static_pages#about'
  get '/contact',         to: 'static_pages#contact'
  get '/info',            to: 'static_pages#info'
  get '/booking',         to: 'static_pages#booking'
  get '/trip1',           to: 'static_pages#trip1'
  get '/trip2',           to: 'static_pages#trip2'
end
